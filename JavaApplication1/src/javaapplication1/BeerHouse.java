/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication1;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author usuario
 */
public class BeerHouse {
    
    List<String> stock;

    public BeerHouse() {
        this.stock = new ArrayList();
    }
    public synchronized void producir(String birra) throws InterruptedException
    {
        while(stock.size()>100)
        {
            try
            {
                wait();
            }
            catch (InterruptedException e) 
            {}
        }
        stock.add(birra);

    }
    public synchronized String consumir() throws InterruptedException
    {
        String birraSacada = null ;
      //  while(this.stock.isEmpty()){
          //  wait();  //codigo elimindo porque si los consumidores esperaban a que alla mas birras, el programa no terminaba nunca, opte porq el productor espere para producir mas
                            // y el consumidor notifique al productor para que continue produciendo.
        //  }
        if(!this.stock.isEmpty()) {
            birraSacada = this.stock.remove(this.stock.size() - 1);
            notifyAll();
        }

        return birraSacada;
    }
    
}
