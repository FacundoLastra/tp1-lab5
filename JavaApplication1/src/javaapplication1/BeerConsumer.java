/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication1;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author usuario
 */
public class BeerConsumer extends Thread {
    
    String name;
    BeerHouse house;

    public BeerConsumer(BeerHouse house, String name) {
        this.house = house;
        this.name=name;
    }
    @Override
    public void run(){
        String birra = null;
        do{
            try {
                birra=this.house.consumir();
                if (birra != null){
                    System.out.println("El consumidor "+this.name+" a consumido la birra : "+birra);
                }
                BeerConsumer.sleep(8);
            } catch (InterruptedException ex) {
                Logger.getLogger(BeerConsumer.class.getName()).log(Level.SEVERE, null, ex);
            }
        }while(!this.house.stock.isEmpty());
        System.out.println("Consumidor "+this.name +" a terminado de Consumir!");
        
        }
    
}
