/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication1;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author usuario
 */
public class BeerProducer extends Thread{
    
    List<String> Cerveza ;
    BeerHouse house;

    public BeerProducer(BeerHouse house,ArrayList<String> cervezas) {
        this.Cerveza = cervezas;
        this.house=house;
    }

    public List<String> getCerveza() {
        return Cerveza;
    }

    public void setCerveza(List<String> Cerveza) {
        this.Cerveza = Cerveza;
    }
    private int getNumeroAleatorio(){
        Random aleatorio = new Random();
        // Producir nuevo int aleatorio, numero limite no esta incluido ej: si ponemos 100 dara numero entre 0 y 99
        int intAletorio = aleatorio.nextInt(this.Cerveza.size()); ///genero numero aleatorio en base a cantidad de libros en la biblioteca
        return intAletorio;
    }
    
    @Override
    public void run(){
        try {
            while(this.house.stock.size()<100){
                String birra = this.Cerveza.get(this.getNumeroAleatorio());
                this.house.producir(birra);
                System.out.println("BeerProducer Agrego la Birra "+ birra);
                BeerProducer.sleep(2);
            }
        } catch (InterruptedException ex) {
            Logger.getLogger(BeerProducer.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("No pudimos meter la birra en el BeerHouse");
        }
        
        
    }
}
