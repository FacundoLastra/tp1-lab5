/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication1;

import java.util.ArrayList;

/**
 *
 * @author usuario
 */
public class JavaApplication1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
       
        ArrayList<String> birras = new ArrayList();
        birras.add("honny");
        birras.add("Seasion IPA");
        birras.add("Patagonia");
        birras.add("Quilmes");
        birras.add("Sol");
        birras.add("Duff");
        BeerHouse house= new BeerHouse();
        
        BeerProducer productor= new BeerProducer(house,birras);
        
        BeerConsumer consumidor1=new BeerConsumer(house,"Consumidor1");
        BeerConsumer consumidor2=new BeerConsumer(house,"Consumidor2");
        BeerConsumer consumidor3=new BeerConsumer(house,"Consumidor3");
        
        productor.start();
        
        consumidor1.start();
        consumidor2.start();
        consumidor3.start();

        
    }
    
}
